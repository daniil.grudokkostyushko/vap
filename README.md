# VUE ACCOUNT PANEL (VAP)

Best Practice of Vue in a form of Vue Dashboard
## Table of Content

**1. Installation**
* 1.1. JavaScript runtime builder
* 1.2. Package manager

**2. Contribution**
* 2.1. Commit Messages
* 2.2. Merge Request

# 1. Installation
## 1.1. JavaScript runtime builder 

- NodeJS (>=10.x)

Source: https://nodejs.org/en/

P.S. Node Version Manager https://github.com/nvm-sh/nvm (Recomended)

## 1.2. Package manager

- yarn 

Source: https://yarnpkg.com/

# 2. Contribution

## 2.1. Commit Messages

Source: https://gist.github.com/stephenparish/9941e89d80e2bc58a153

### Types

The first part of the commit message 

| Definition | Description  |
|-----------------|:-------------|
| feat     | feature |
| fix      | fix bugs 
| docs     | documentation |
| style    | formatting, missing semi colons |
| visual   | scss filed 
| refactor | replacing or fix without any changes for user |
| test     | adding tests |
| build    | new dependance 
| chore    | maintain, upgrades |
| revert   | Reverts a previous commit |

Example: feat: add familyname textbox in Registration.vue

### Scope

The second part of the commit message

| Definition | Description  |
|-----------------|:-------------|
| hp    | Home Page |
| cp    | Category Page |
| srp   | Search Result Page |
| pdp   | Product Detail Page |

Example: feat(hp), fix(pdp)

### Body

The third part of the commit message (description)

| Definition | Description  |
|-----------------|:-------------|
| set       | fix(srp): correct search bar for Product.vue |
| add       | feat(pdp): add BoxPricing.vue |
| remove    | test(pdp): remove old test for a BoxPricing.vue |
| correct   | refactor(pdp): correct path for Product.vue inside Home.vue |

## 2.2. Merge Request

### Open MR

- master
- feature/title
- release/title
- hotfix/title

### Review of MR

#### 1. Formality

- Correct name of the branch
- Correct commits
- Correct syntax of changes

#### 2. Visual Changes

- Correct visual changes (UX/UI)**

#### 3. Functional Changes

- Browser console
- Refresh the page

#### Conclusions

If everything correct comment the MR with following message

| Formality | Visual Changes | Functional Changes |
| ------ | ------ | ------ |
| ✓ correct branch name | ✓ correct css syntax | ✓ correct js syntax
| ✓ correct commits  |   | ✓ correct refreshing the page
| ✓ correct code syntax  |   | ✓ correct configuration